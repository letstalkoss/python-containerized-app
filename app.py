from flask import Flask, render_template, request, flash
from modules import articles

app = Flask(__name__)
app.secret_key = b'\xa5w\xd8j\x05\xb2\xe3y\xc2\xba\xff/&\x9e\x19\xaa'

@app.route('/', methods = ['GET'])
def index():
    a = articles.get_articles()
    return render_template('index.html', a = a)

if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = '8080')
