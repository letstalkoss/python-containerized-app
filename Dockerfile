FROM python
COPY requirements.txt requirements.txt
RUN pip3 install --upgrade pip --no-cache-dir
RUN pip3 install -r /requirements.txt --no-cache-dir

COPY . /app
WORKDIR /app

EXPOSE 8080

CMD ["gunicorn","--config", "gunicorn_config.py", "app:app"]
