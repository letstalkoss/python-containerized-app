import requests
import json

def get_articles():
    articles = requests.get('https://dev.to/api/articles?username=mattdark')
    jsonResponse = articles.json()
    return jsonResponse
